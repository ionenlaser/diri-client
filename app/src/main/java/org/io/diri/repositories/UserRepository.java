package org.io.diri.repositories;

import android.app.Application;

import androidx.lifecycle.LiveData;

import org.io.diri.data.DiriDatabase;
import org.io.diri.data.dao.UserDao;
import org.io.diri.data.entities.User;

public class UserRepository {
    private final UserDao userDao;
    private final LiveData<User> user;

    public UserRepository(Application application) {
        DiriDatabase diriDatabase = DiriDatabase.getInstance(application);
        userDao = diriDatabase.userDao();
        user = userDao.loadUserData();
    }

    public LiveData<User> getAllUsers() {
        return user;
    }

    public void insertUser(User user) {
        DiriDatabase.databaseExecutor.execute(() -> userDao.insertUser(user));
    }

    public void updateUser(User user) {
        DiriDatabase.databaseExecutor.execute(() -> userDao.updateUser(user));
    }
}



