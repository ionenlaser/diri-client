package org.io.diri.repositories;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.io.diri.activities.MainActivity;
import org.io.diri.data.DiriDatabase;
import org.io.diri.data.dao.SpellingDao;
import org.io.diri.data.entities.Spelling;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

public class SpellingRepository {
    private static final String LOG_TAG = "SpellingRepository";
    private final SpellingDao spellingDao;
    private final Context context;
    private final LiveData<List<Spelling>> unplayedSpellings;
    private final RequestQueue queue;


    public SpellingRepository(Application application) {
        DiriDatabase diriDatabase = DiriDatabase.getInstance(application);
        spellingDao = diriDatabase.spellingDao();
        unplayedSpellings = spellingDao.loadUnplayedSpellingData();
        context = application.getApplicationContext();
        queue = MainActivity.queue;
    }

    public void insert(Spelling spelling) {
        DiriDatabase.databaseExecutor.execute(() -> spellingDao.insertSpelling(spelling));
    }

    public void update(Spelling spelling) {
        Log.i(LOG_TAG, "Update Spelling, not threaded");
        DiriDatabase.databaseExecutor.execute(() -> spellingDao.updateSpelling(spelling));
    }

    public LiveData<List<Spelling>> getUnplayedSpellings() {
        return unplayedSpellings;
    }

    public void downloadSpelling() {
        Spelling newSpelling;
        try {
            String urlString = String.format(MainActivity.baseUrl + "spelling?id=%s",
                    MainActivity.user_acc.getId());
            URL url = new URL(urlString);
            Log.i(LOG_TAG, "Connect to this url: " + url);

            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
            String filenameHeader = conn.getHeaderField("Content-Disposition");

            if (conn.getResponseCode() > 201) {
                String errorString = "ResponseCode Error detected: " + conn.getResponseCode();
                throw new IOException(errorString);
            }

            Log.i(LOG_TAG, "FilenameHeader is: " + filenameHeader);
            if (filenameHeader == null) {
                Log.i(LOG_TAG, conn.toString());
                throw new IOException("FilenameHeader not included");
            }
            int index = filenameHeader.lastIndexOf('=');
            int server_id = Integer.parseInt(filenameHeader.substring(index + 1));

            Log.i(LOG_TAG, "Server_id is: " + server_id);

            int contentLength = conn.getContentLength();
            Log.i(LOG_TAG, "Content length is: " + contentLength);
            //Log.i(LOG_TAG, "Other Headers: " + conn.getHeaderFields());


            List<Byte> byteList = new ArrayList<>();
            DataInputStream stream = new DataInputStream(conn.getInputStream());

            while (true) {
                try {
                    Byte b = stream.readByte();
                    byteList.add(b);
                } catch (EOFException e) {
                    Log.i(LOG_TAG, "End of file reached at: " + byteList.size());
                    break;
                }
            }

            Byte[] byteArray = byteList.toArray(new Byte[byteList.size()]);
            stream.close();

            //Datei wird in dateisystem (client) gespeichert
            String filename = UUID.randomUUID().toString() + ".3gp";
            String filePath = context.getFilesDir().getAbsolutePath() + "/" + filename;
            DataOutputStream fos = new DataOutputStream(new FileOutputStream(filePath));
            for (Byte aByte : byteArray) {
                fos.writeByte(aByte);
            }
            fos.flush();
            fos.close();

            newSpelling = new Spelling(server_id, filename);
            insert(newSpelling);
            getAdditionalInfo(server_id);
        } catch (IOException e) {
            Log.e(LOG_TAG, "IOException exception");
            e.printStackTrace();
        }
    }

    public void getAdditionalInfo(int serverID) {
        // Instantiate the RequestQueue.
        String url = String.format(MainActivity.baseUrl + "spelling?id=%s&spelling_id=%s",
                MainActivity.user_acc.getId(), serverID);


        DiriDatabase.databaseExecutor.execute(() -> {
            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    response -> {
                        try {
                            JSONObject responseObj = new JSONObject(response);
                            String lexeme = responseObj.getString("lexeme");
                            String language = responseObj.getString("lang");
                            String ipa = responseObj.getString("ipa");
                            Log.i(LOG_TAG, "Additional data received, content: " + lexeme);
                            Spelling spelling = spellingDao.loadSpellingByServerID(serverID);
                            spelling.setLexeme(lexeme);
                            spelling.setIpa(ipa);
                            spelling.setLanguage(language);
                            update(spelling);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }, error -> {
                Log.e(LOG_TAG, "Volley error occurred!: " + error.toString());
                Log.e(LOG_TAG, "Error content: " + Arrays.toString(error.networkResponse.data));
            });
            Log.i(LOG_TAG, "Download additional info");
            // Add the request to the RequestQueue.
            queue.add(stringRequest);
        });
    }


    public void postSpellingInput() {
        String requestURL = MainActivity.baseUrl + "spelling";

        JSONObject object = new JSONObject();
        DiriDatabase.databaseExecutor.execute(() -> {
            try {
                object.put("client_id", MainActivity.user_acc.getId());
                JSONArray array = new JSONArray();
                List<Spelling> spellingList = spellingDao.loadNotUploadedSpellings();
                for (int i = 0; i < spellingList.size(); i++) {
                    Spelling spelling = spellingList.get(i);
                    JSONObject spellingObj = new JSONObject();
                    spellingObj.put("skipped", spelling.isSkipped());
                    spellingObj.put("id", spelling.getServerid());
                    spellingObj.put("inputs", spelling.getInputs());
                    array.put(spellingObj);
                    spelling.setUploaded(true);
                    spellingDao.updateSpelling(spelling);
                }
                object.put("spellings", array);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.i(LOG_TAG, "This JSON will be sent: " + object);

            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, requestURL, object,
                    response -> Log.i(LOG_TAG, response.toString()), error -> Log.e(LOG_TAG, error.toString()));
            queue.add(jsonObjectRequest);
        });
    }
}


