package org.io.diri.repositories;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.android.volley.Request;
import com.android.volley.toolbox.StringRequest;

import org.io.diri.activities.MainActivity;
import org.io.diri.data.DiriDatabase;
import org.io.diri.data.dao.PronunciationDao;
import org.io.diri.data.entities.Pronunciation;
import org.io.diri.network.MultipartUtility;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.List;


public class PronunciationRepository {
    private static final String LOG_TAG = "PronunciationRepository";
    private final PronunciationDao pronunciationDao;
    private final LiveData<List<Pronunciation>> unplayedPronunciations;
    private final Context context;


    // See the BasicSample in the android-architecture-components repository at
    // https://github.com/googlesamples

    public PronunciationRepository(Application application) {
        DiriDatabase diriDatabase = DiriDatabase.getInstance(application);
        context = application.getApplicationContext();
        pronunciationDao = diriDatabase.pronunciationDao();
        unplayedPronunciations = pronunciationDao.loadUnplayedPronunciationData();
    }

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    public LiveData<List<Pronunciation>> getUnplayedPronunciations() {
        return unplayedPronunciations;
    }

    // You must call this on a non-UI thread or your app will throw an exception. Room ensures
    // that you're not doing any long running operations on the main thread, blocking the UI.
    public void insert(Pronunciation pronunciation) {
        DiriDatabase.databaseExecutor.execute(() -> pronunciationDao.insertPronunciation(pronunciation));
    }

    public void update(Pronunciation pronunciation) {
        DiriDatabase.databaseExecutor.execute(() -> pronunciationDao.updatePronunciation(pronunciation));
    }

    public void downloadPronunciationData() {
        String url = String.format(MainActivity.baseUrl + "pronunciation?id=%s",
                MainActivity.user_acc.getId());

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    try {
                        JSONObject responseObj = new JSONObject(response);
                        savePtoDB(responseObj);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            Log.e(LOG_TAG, "Pronunciations download failed!");
            MainActivity.toast(context, "Pronunciations download failed!");
        });
        // Add the request to the RequestQueue.
        MainActivity.queue.add(stringRequest);
    }

    private void savePtoDB(JSONObject responseObj) throws JSONException {
        Log.i(LOG_TAG, responseObj.toString());
        JSONArray pronunciationsArray = responseObj.getJSONArray("pronunciations");
        for (int i = 0; i < pronunciationsArray.length(); i++) {
            JSONObject obj = pronunciationsArray.getJSONObject(i);
            Pronunciation newPronunciation = new Pronunciation(
                    obj.getInt("id"),
                    obj.getString("ipa"),
                    obj.getString("lexeme"),
                    obj.getString("lang")
            );
            insert(newPronunciation);
        }
    }


    public void postPronunciationData(Pronunciation pronunciation) throws IOException, JSONException {
        String charset = "UTF-8";
        String requestURL = MainActivity.baseUrl + "pronunciation";
        String responseString;
        String relativeFileName = "";
        MultipartUtility multipart = new MultipartUtility(requestURL, charset);

        JSONObject pronunciationObj = new JSONObject();
        pronunciationObj.put("pro_id", pronunciation.getServer_id());
        pronunciationObj.put("skipped", pronunciation.getSkipped());
        if (pronunciation.getSkipped()) {
            pronunciationObj.put("f_name", "");
        } else {
            int index = pronunciation.getRecording().lastIndexOf('/');
            relativeFileName = pronunciation.getRecording().substring(index + 1);
            pronunciationObj.put("f_name", relativeFileName);
        }

        JSONArray proArray = new JSONArray();
        proArray.put(pronunciationObj);

        JSONObject infoObj = new JSONObject();
        infoObj.put("client_id", MainActivity.user_acc.id);
        infoObj.put("pronunciations", proArray);
        multipart.addFormField("info", infoObj.toString());

        if (!relativeFileName.equals("")) {
            multipart.addFilePart(relativeFileName,
                    new File(pronunciation.getRecording()));
        }

        List<String> response = multipart.finish();
        for (String line : response) {
            Log.i(LOG_TAG, "Upload Files Response:" + line);
            // get your server response here.
            responseString = line;
            Log.i(LOG_TAG, responseString);
        }
    }
}
