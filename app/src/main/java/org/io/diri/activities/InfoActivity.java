package org.io.diri.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import org.io.diri.R;

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
    }
}
