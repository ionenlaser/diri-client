package org.io.diri.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;

import org.io.diri.R;
import org.io.diri.data.entities.Spelling;
import org.io.diri.data.entities.User;
import org.io.diri.viewModels.SpellingViewModel;

import java.io.File;
import java.util.List;
import java.util.Objects;
import java.util.Random;


public class SpellActivity extends AppCompatActivity {

    private static final String LOG_TAG = "SpellActivity";
    private SpellingViewModel spellingViewModel;
    private List<Spelling> allSpellings;
    private Spelling activeSpelling;
    private String activeFileName;
    private EditText userInput;
    private Boolean initial = true;
    private MediaPlayer activeMediaPlayer;
    private Button soundButton;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spell);
        spellingViewModel = new ViewModelProvider(this).get(SpellingViewModel.class);

        soundButton = findViewById(R.id.buttonSound);
        soundButton.setOnClickListener(v -> {
            activeMediaPlayer = new MediaPlayer();

            Log.i(LOG_TAG, "Playing this file: " + activeFileName);
            File activeFile = new File(activeFileName);
            Log.i(LOG_TAG, "File exists: " + activeFile.exists());
            Log.i(LOG_TAG, "File size: " + activeFile.length());
            activeMediaPlayer = MediaPlayer.create(getApplicationContext(), Uri.parse(activeFile.getAbsolutePath()));
            activeMediaPlayer.start();
        });

        spellingViewModel.getAllSpellings().observe(this, spellings -> {
            // Update the cached copy of the words in the adapter.
            if (spellings.size() > 0) {
                allSpellings = spellings;
                if (initial) {
                    initial = false;
                    nextItem();
                }
                if (spellings.size() < 3) {
                    spellingViewModel.downloadNumberOfSpellings(5);
                }
            } else {
                tooFewSpellings();
            }
        });

        final Button skipButton = findViewById(R.id.buttonSkip2);
        skipButton.setOnClickListener(v -> {
            activeSpelling.setSkipped(true);
            spellingViewModel.update((activeSpelling));
            MainActivity.toast(getApplicationContext(), "The right answer is " + activeSpelling.getLexeme());
            nextItem();
        });

        userInput = findViewById(R.id.editTextUserInput);

        final Button checkButton = findViewById(R.id.buttonCheck);
        checkButton.setOnClickListener(v -> {
            String inputStr = userInput.getText().toString();

            Log.i(LOG_TAG, "This is the spelling list: ");
            for (int i = 0; i < allSpellings.size(); i++) {
                Log.i(LOG_TAG, "Id: " + allSpellings.get(i).getId() + " File: " + allSpellings.get(i).getRecording() + " Lexeme: " + allSpellings.get(i).getLexeme());
            }


            if (inputStr.equalsIgnoreCase(activeSpelling.getLexeme())) {
                Log.i(LOG_TAG, "User input: " + inputStr);
                MainActivity.toast(getApplicationContext(), "That is correct, +1 point");
                activeSpelling.setInputs(inputStr);
                activeSpelling.setSkipped(false);
                spellingViewModel.update(activeSpelling);

                User user = MainActivity.user_acc;
                int points = user.getPoints();
                user.setPoints(points + 1);
                MainActivity.userViewModel.updateUser(user);
                // Upload spelling data
                spellingViewModel.postSpellingData();
                nextItem();
            } else {
                Log.i(LOG_TAG, "Wrong Input: " + inputStr + " Wanted: " + activeSpelling.getLexeme());
                MainActivity.toast(getApplicationContext(), "Incorrect, Try again!");
                String oldInputs = activeSpelling.getInputs();
                activeSpelling.setInputs(oldInputs + "," + inputStr);
            }
        });

        final Button downloadButton = findViewById(R.id.buttonDownload);
        downloadButton.setOnClickListener(v -> {
            try {
                spellingViewModel.downloadNumberOfSpellings(1);
            } catch (Exception e) {
                Log.e(LOG_TAG, "Catching exception, trying to display alert message!");
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (activeMediaPlayer != null) {
            activeMediaPlayer.release();
            activeMediaPlayer = null;
        }
    }

    private void nextItem() {
        if (activeMediaPlayer != null) {
            activeMediaPlayer.release();
            activeMediaPlayer = null;
        }
        userInput.setText("");
        Log.i(LOG_TAG, "Number of available spellings: " + allSpellings.size());
        if (allSpellings.size() < 1) {
            tooFewSpellings();
            return;
        }
        Random rand = new Random();
        int nextSpellingNumber = rand.nextInt(allSpellings.size());
        Log.i(LOG_TAG, "Random next spelling: " + nextSpellingNumber);

        Spelling spelling = allSpellings.get(nextSpellingNumber);
        if (spelling == null) {
            Log.e(LOG_TAG, "No more spellings available");
            MainActivity.toast(getApplicationContext(), "No more spellings available");
            return;
        }
        activeSpelling = spelling;
        activeFileName = getFilesDir().getAbsolutePath() + "/" + activeSpelling.getRecording();
        Log.i(LOG_TAG, "Selected this absolute filepath: " + activeFileName);
        // These seem to work: /data/user/0/org.io.diri/files/82893.m4a
    }

    private void tooFewSpellings() {
        Log.w(LOG_TAG, "Too few spellings!");
        findViewById(R.id.buttonPanel).setVisibility(View.INVISIBLE);
        soundButton.setVisibility(View.INVISIBLE);
        TextView taskTextView = findViewById(R.id.textViewTask);
        taskTextView.setText(R.string.NoMoreSpellings);
        findViewById(R.id.buttonDownload).setVisibility(View.VISIBLE);
    }
}