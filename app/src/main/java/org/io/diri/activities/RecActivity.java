package org.io.diri.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.ViewModelProvider;

import org.io.diri.R;
import org.io.diri.data.entities.Pronunciation;
import org.io.diri.data.entities.User;
import org.io.diri.viewModels.PronunciationViewModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Random;


public class RecActivity extends AppCompatActivity {

    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private static final String LOG_TAG = "AudioRecordTest";
    private final String[] permissions = {Manifest.permission.RECORD_AUDIO};
    private final ViewGroup.LayoutParams lp = new LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT, 1.0f);
    private boolean permissionToRecordAccepted = false;
    private MediaRecorder recorder = null;
    private String activeFileName;
    private PronunciationViewModel pronunciationViewModel;
    private TextView textViewPronunciation;
    private List<Pronunciation> pronunciationList;
    private Pronunciation activePronunciation;
    private MediaPlayer activeMediaPlayer;
    private Button playRecButton;
    private Button confirmButton;
    private LinearLayout verticalBox;
    private JSONArray ipaSounds;

    private static JSONArray getIPASounds() {
        String all = "{\"content\": [" +
                "[\"bilabial_nasal\", \"m\"]," +
                "[\"labiodental_nasal\", \"ɱ\"]," +
                "[\"alveolar_nasal\", \"n\"]," +
                "[\"retroflex_nasal\", \"ɳ\"]," +
                "[\"palatal_nasal\", \"ɲ\"]," +
                "[\"velar_nasal\", \"ŋ\"]," +
                "[\"uvular_nasal\", \"ɴ\"]," +
                "[\"voiceless_bilabial_plosive\", \"p\"]," +
                "[\"voiced_bilabial_plosive\", \"b\"]," +
                "[\"voiceless_alveolar_plosive\", \"t\"]," +
                "[\"voiced_alveolar_plosive\", \"d\"]," +
                "[\"voiceless_retroflex_stop\", \"ʈ\"]," +
                "[\"voiced_retroflex_stop\", \"ɖ\"]," +
                "[\"voiceless_palatal_plosive\", \"c\"]," +
                "[\"voiced_palatal_plosive\", \"ɟ\"]," +
                "[\"voiceless_velar_plosive\", \"k\"]," +
                "[\"voiced_velar_plosive\", \"g\"]," +
                "[\"voiceless_uvular_plosive\", \"q\"]," +
                "[\"voiced_uvular_stop\", \"ɢ\"]," +
                "[\"glottal_stop\", \"ʔ\"]," +
                "[\"voiceless_bilabial_fricative\", \"ɸ\"]," +
                "[\"voiced_bilabial_fricative\", \"β\"]," +
                "[\"voiceless_labiodental_fricative\", \"f\"]," +
                "[\"voiced_labiodental_fricative\", \"v\"]," +
                "[\"voiceless_dental_fricative\", \"θ\"]," +
                "[\"voiced_dental_fricative\", \"ð\"]," +
                "[\"voiceless_alveolar_fricative\", \"s\"]," +
                "[\"voiced_alveolar_fricative\", \"z\"]," +
                "[\"voiceless_postalveolar_fricative\", \"ʃ\"]," +
                "[\"voiced_postalveolar_fricative\", \"ʒ\"]," +
                "[\"voiceless_retroflex_fricative\", \"ʂ\"]," +
                "[\"voiced_retroflex_fricative\", \"ʐ\"]," +
                "[\"voiceless_palatal_fricative\", \"ç\"]," +
                "[\"voiced_palatal_fricative\", \"ʝ\"]," +
                "[\"voiceless_velar_fricative\", \"x\"]," +
                "[\"voiced_velar_fricative\", \"ɣ\"]," +
                "[\"voiceless_uvular_fricative\", \"χ\"]," +
                "[\"voiced_uvular_fricative\", \"ʁ\"]," +
                "[\"voiceless_pharyngeal_fricative\", \"ħ\"]," +
                "[\"voiced_pharyngeal_fricative\", \"ʕ\"]," +
                "[\"voiceless_glottal_fricative\", \"h\"]," +
                "[\"voiced_glottal_fricative\", \"ɦ\"]," +
                "[\"labiodental_approximant\", \"ʋ\"]," +
                "[\"labiodental_flap\", \"ⱱ\"]," +
                "[\"alveolar_approximant\", \"ɹ\"]," +
                "[\"retroflex_approximant\", \"ɻ\"]," +
                "[\"palatal_approximant\", \"j\"]," +
                "[\"voiced_velar_approximant\", \"ɰ\"]," +
                "[\"bilabial_trill\", \"ʙ\"]," +
                "[\"alveolar_trill\", \"r\"]," +
                "[\"uvular_trill\", \"ʀ\"]," +
                "[\"alveolar_tap\", \"ɾ\"]," +
                "[\"retroflex_flap\", \"ɽ\"]," +
                "[\"voiceless_alveolar_lateral_fricative\", \"ɬ\"]," +
                "[\"voiced_alveolar_lateral_fricative\", \"ɮ \"]," +
                "[\"alveolar_lateral_approximant\", \"l\"]," +
                "[\"retroflex_lateral_approximant\", \"ɭ\"]," +
                "[\"palatal_lateral_approximant\", \"ʎ\"]," +
                "[\"velar_lateral_approximant\", \"ʟ\"]," +
                "[\"close_back_rounded_vowel\", \"u\"]," +
                "[\"close_back_unrounded_vowel\", \"ɯ\"]," +
                "[\"close_central_rounded_vowel\", \"ʉ\"]," +
                "[\"close_central_unrounded_vowel\", \"ɨ\"]," +
                "[\"close_front_rounded_vowel\", \"y\"]," +
                "[\"close_front_unrounded_vowel\", \"i\"]," +
                "[\"close_mid_back_rounded_vowel\", \"o\"]," +
                "[\"close_mid_back_unrounded_vowel\", \"ɤ\"]," +
                "[\"close_mid_central_rounded_vowel\", \"ɵ\"]," +
                "[\"close_mid_central_unrounded_vowel\", \"ɘ\"]," +
                "[\"close_mid_front_rounded_vowel\", \"ø\"]," +
                "[\"close_mid_front_unrounded_vowel\", \"e\"]," +
                "[\"mid_central_vowel\", \"ə\"]," +
                "[\"near_close_nearback_rounded_vowel\", \"ʊ\"]," +
                "[\"near_close_nearfront_rounded_vowel\", \"ʏ\"]," +
                "[\"near_close_nearfront_unrounded_vowel\", \"ɪ\"]," +
                "[\"near_open_central_unrounded_vowel\", \"ɐ\"]," +
                "[\"near_open_front_unrounded_vowel\", \"æ\"]," +
                "[\"open_back_rounded_vowel\", \"ɒ\"]," +
                "[\"open_back_unrounded_vowel\", \"ɑ\"]," +
                "[\"open_front_rounded_vowel\", \"ɶ\"]," +
                "[\"open_front_unrounded_vowel\", \"a\"]," +
                "[\"open_mid_back_rounded_vowel\", \"ɔ\"]," +
                "[\"open_mid_back_unrounded_vowel\", \"ʌ\"]," +
                "[\"open_mid_central_rounded_vowel\", \"ɞ\"]," +
                "[\"open_mid_central_unrounded_vowel\", \"ɜ\"]," +
                "[\"open_mid_front_rounded_vowel\", \"œ\"]," +
                "[\"open_mid_front_unrounded_vowel\", \"ɛ\"]," +
                "[\"voiceless_labio_velar_fricative\", \"ʍ\"]," +
                "[\"voiced_labio_velar_approximant\", \"w\"]," +
                "[\"velarized_alveolar_lateral_approximant\", \"ɫ\"]," +
                "[\"voiceless_epiglottal_trill\", \"ʜ\"]," +
                "[\"voiced_epiglottal_trill_2\", \"ʢ\"]," +
                "[\"voiceless_alveolo_palatal_sibilant\", \"ɕ\"]," +
                "[\"voiced_alveolo_palatal_sibilant\", \"ʑ\"]," +
                "[\"voiceless_dorso_palatal_velar_fricative\", \"ɧ\"]," +
                "[\"voiceless_alveolar_sibilant_affricate\", \"t͡s\"]," +
                "[\"voiceless_alveolar_sibilant_affricate\", \"ʦ\"]," +
                "[\"voiceless_palato_alveolar_affricate\", \"t͡ʃ\"]," +
                "[\"voiceless_palato_alveolar_affricate\", \"ʧ\"]," +
                "[\"voiceless_alveolo_palatal_affricate\", \"t͡ɕ\"]," +
                "[\"voiceless_alveolo_palatal_affricate\", \"ʨ\"]," +
                "[\"voiceless_retroflex_affricate\", \"ʈ͡ʂ\"]," +
                "[\"voiceless_retroflex_affricate\", \"tʂ\"]," +
                "[\"voiceless_retroflex_affricate\", \"ʈʂ\"]," +
                "[\"voiced_alveolar_sibilant_affricate\", \"d͡z\"]," +
                "[\"voiced_alveolar_sibilant_affricate\", \"ʣ\"]," +
                "[\"voiced_palato_alveolar_affricate\", \"d͡ʒ\"]," +
                "[\"voiced_palato_alveolar_affricate\", \"ʤ\"]," +
                "[\"voiced_retroflex_affricate\", \"ɖ͡ʐ\"]," +
                "[\"voiced_retroflex_affricate\", \"ɖʐ\"]," +
                "[\"voiced_alveolo_palatal_affricate\", \"d͡ʑ\"]," +
                "[\"voiced_alveolo_palatal_affricate\", \"ʥ\"]," +
                "[\"bilabial_click\", \"ʘ\"]," +
                "[\"dental_click\", \"ǀ\"]," +
                "[\"postalveolar_click\", \"ǃ\"]," +
                "[\"palatoalveolar_click\", \"ǂ\"]," +
                "[\"alveolar_lateral_click\", \"ǁ\"]," +
                "[\"voiced_bilabial_implosive\", \"ɓ\"]," +
                "[\"voiced_alveolar_implosive\", \"ɗ\"]," +
                "[\"voiced_palatal_implosive\", \"ʄ\"]," +
                "[\"voiced_velar_implosive\", \"ɠ\"]," +
                "[\"voiced_uvular_implosive\", \"ʛ\"]," +
                "[\"bilabial_ejective_plosive\", \"pʼ\"]," +
                "[\"alveolar_ejective_plosive\", \"tʼ\"]," +
                "[\"retroflex_ejective_plosive\", \"ʈʼ\"]," +
                "[\"velar_ejective_plosive\", \"kʼ\"]," +
                "[\"uvular_ejective_plosive\", \"qʼ\"]," +
                "[\"labiodental_ejective_fricative\", \"fʼ\"]," +
                "[\"dental_ejective_fricative\", \"θʼ\"]," +
                "[\"alveolar_ejective_fricative\", \"sʼ\"]," +
                "[\"retroflex_ejective_fricative\", \"ʂʼ\"]," +
                "[\"palatal_ejective_fricative\", \"çʼ\"]," +
                "[\"velar_ejective_fricative\", \"xʼ\"]," +
                "[\"uvular_ejective_fricative\", \"χʼ\"]," +
                "]}";
        try {
            JSONObject obj = new JSONObject(all);
            JSONArray sounds = obj.getJSONArray("content");
            Log.i(LOG_TAG, "IPAJson: " + sounds);
            return sounds;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rec);
        textViewPronunciation = findViewById(R.id.textViewPronunciation);
        //ipaLayoutBox = (LinearLayout)findViewById(R.id.IPALayoutBox);
        verticalBox = findViewById(R.id.VerticalBox);
        //fileName = getExternalCacheDir().getAbsolutePath();

        ipaSounds = getIPASounds();
        // Get a new or existing ViewModel from the ViewModelProvider.
        pronunciationViewModel = new ViewModelProvider(this).get(PronunciationViewModel.class);

        // Add an observer on the LiveData returned by getAlphabetizedWords.
        // The onChanged() method fires when the observed data changes and the activity is
        // in the foreground.
        pronunciationViewModel.getUnplayedPronunciations().observe(this, pronunciations -> {
            // Update the cached copy of the words in the adapter.
            if (pronunciations.size() == 1) {
                pronunciationViewModel.downloadPronunciations();
            }
            if (pronunciations.size() > 0) {
                Log.w(LOG_TAG, "Pronunciations loaded: " + pronunciations.size());
                pronunciationList = pronunciations;
                nextItem();
            } else {
                tooFewIPAs();
                Log.w(LOG_TAG, "No pronunciations loaded");
                textViewPronunciation.setText("There is no more data at the moment");
            }
        });

        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);

        final Button recordButton = findViewById(R.id.buttonRecord);
        recordButton.setOnTouchListener((v, event) -> {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    startRecording();
                    Log.i(LOG_TAG, "Start recording of " + activeFileName);
                    return true;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    stopRecording();
                    return true;
            }
            return false;
        });

        playRecButton = findViewById(R.id.buttonPlayRec);
        playRecButton.setEnabled(false);
        playRecButton.setOnClickListener(v -> {
            activeMediaPlayer = MediaPlayer.create(getApplicationContext(), Uri.parse(activeFileName));
            activeMediaPlayer.start();
            Log.i(LOG_TAG, "Uri: " + Uri.parse(activeFileName));
        });

        final Button skipButton = findViewById(R.id.buttonSkip);
        skipButton.setOnClickListener(v -> {
            activePronunciation.setSkipped(true);
            pronunciationViewModel.postPronunciations(activePronunciation);
            pronunciationViewModel.update(activePronunciation);
            nextItem();
        });

        final Button hintButton = findViewById(R.id.buttonHint);
        hintButton.setOnClickListener(v -> {
           String hintlexeme = activePronunciation.getLexeme();
           String displaybox = (String) textViewPronunciation.getText();
           if (displaybox.equals(activePronunciation.getIpa())){
            //TextView pronunciationviewText = findViewById(R.id.textViewPronunciation);
            textViewPronunciation.setText(hintlexeme);
            findViewById(R.id.VerticalBox).setVisibility(View.INVISIBLE);
           }
            if (displaybox.equals(activePronunciation.getLexeme())){
                //TextView pronunciationviewText = findViewById(R.id.textViewPronunciation);
                textViewPronunciation.setText(activePronunciation.getIpa());
                findViewById(R.id.VerticalBox).setVisibility(View.VISIBLE);
           }
        });

        confirmButton = findViewById(R.id.buttonConfirm);

        confirmButton.setEnabled(false);
        confirmButton.setOnClickListener(v -> {
            Log.i(LOG_TAG, "Saving file to DB: " + activeFileName);
            MainActivity.toast(getApplicationContext(), "Thank you, +1 point");
            activePronunciation.setRecording(activeFileName);
            activePronunciation.setSkipped(false);
            pronunciationViewModel.postPronunciations(activePronunciation);
            pronunciationViewModel.update(activePronunciation);

            User user = MainActivity.user_acc;
            int points = user.getPoints();
            int recs = user.getRecordings();
            user.setPoints(points+1);
            user.setRecordings(recs+1);
            MainActivity.userViewModel.updateUser(user);
        });


        final Button dataButton = findViewById(R.id.buttonData);
        dataButton.setOnClickListener(v -> pronunciationViewModel.downloadPronunciations());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (activeMediaPlayer != null) {
            activeMediaPlayer.release();
            activeMediaPlayer = null;
        }
    }

    private void nextItem() {
        verticalBox.removeAllViews();
        verticalBox.setVisibility(View.VISIBLE);
        if (activeMediaPlayer != null) {
            activeMediaPlayer.release();
            activeMediaPlayer = null;
        }
        Log.i(LOG_TAG, "Number of available IPAs: " + pronunciationList.size());
        if (pronunciationList.size() < 1) {
            tooFewIPAs();
            return;
        }

        Random rand = new Random();
        activePronunciation = pronunciationList.get(rand.nextInt(pronunciationList.size()));
        textViewPronunciation.setText(activePronunciation.getIpa());
        playRecButton.setEnabled(false);
        playRecButton.setVisibility(View.INVISIBLE);
        confirmButton.setEnabled(false);
        confirmButton.setVisibility(View.INVISIBLE);
        activeFileName = generateFilename();
        LinearLayout horizontalLayout = new LinearLayout(this);
        horizontalLayout.setOrientation(LinearLayout.HORIZONTAL);
        verticalBox.addView(horizontalLayout);

        Log.i(LOG_TAG, "Selected IPA: " + activePronunciation.getIpa());
        int columnWidthCounter = 0;
        for (int i = 0; i < activePronunciation.getIpa().length(); i++) {
            if (columnWidthCounter > 5) {
                horizontalLayout = new LinearLayout(this.getApplicationContext());
                verticalBox.addView(horizontalLayout);
                columnWidthCounter = 0;
            } else {
                columnWidthCounter++;
            }
            Character symbol = activePronunciation.getIpa().charAt(i);
            String fileName = "test";
            try {
                for (int j = 0; j < ipaSounds.length() - 1; j++) {
                    String testSymbol = ipaSounds.getJSONArray(j).get(1).toString();
                    if (symbol.toString().equals(testSymbol)) {
                        fileName = ipaSounds.getJSONArray(j).get(0).toString();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (fileName.equals("test")) {
                // Todo: debug this!
                Log.w(LOG_TAG, "Character not found in alphabet");
            } else {
                Button newButton = createButton(i, symbol, fileName);
                horizontalLayout.addView(newButton);
            }
        }
    }

    private void tooFewIPAs() {
        Log.w(LOG_TAG, "Too few IPAs!");
        findViewById(R.id.allButtonsLayout).setVisibility(View.INVISIBLE);
        textViewPronunciation.setVisibility(View.INVISIBLE);
        findViewById(R.id.VerticalBox).setVisibility(View.INVISIBLE);
        TextView viewText = findViewById(R.id.textView);
        viewText.setText(R.string.NoMoreSpellings);
        findViewById(R.id.buttonData).setVisibility(View.VISIBLE);
    }

    private Button createButton(int i, Character symbol, String fileName) {
        Button newButton = new Button(this);
        newButton.setLayoutParams(lp);
        newButton.setAllCaps(false);
        newButton.setId(i);
        newButton.setText(symbol.toString());

        newButton.setOnClickListener(v -> {
            activeMediaPlayer = MediaPlayer.create(getApplicationContext(),
                    getApplicationContext().getResources().getIdentifier(fileName, "raw", getApplicationContext().getPackageName()));
            activeMediaPlayer.start();
        });
        return newButton;
    }

    private String generateFilename() {
        String Path = getApplicationContext().getFilesDir().getAbsolutePath();
        int number = (int) (Math.random() * (100000 + 1) + 0);
        return Path + "/" + number + ".3gp";
    }

    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(activeFileName);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        recorder.setAudioEncodingBitRate(384000);
        recorder.setAudioSamplingRate(44100);
        try {
            recorder.prepare();
            recorder.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() or start() failed");
        }
    }

    private void stopRecording() {
        try {
            recorder.stop();
            playRecButton.setEnabled(true);
            confirmButton.setEnabled(true);
            confirmButton.setVisibility(View.VISIBLE);
            playRecButton.setVisibility(View.VISIBLE);
        } catch (RuntimeException e) {
            playRecButton.setEnabled(false);
            confirmButton.setEnabled(false);
            confirmButton.setVisibility(View.INVISIBLE);
            Log.e(LOG_TAG, "Couldn't stop recording, probably to short");
        }
        recorder.release();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted) finish();
    }
}
