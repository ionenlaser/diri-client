package org.io.diri.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.room.Room;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.io.diri.R;
import org.io.diri.data.DiriDatabase;
import org.io.diri.data.entities.User;
import org.io.diri.viewModels.PronunciationViewModel;
import org.io.diri.viewModels.SpellingViewModel;
import org.io.diri.viewModels.UserViewModel;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    // Toolforge:
    final public static String baseUrl = "https://diri.toolforge.org/";
    // OTHER:
    //final public static String baseUrl = "http://192.168.1.98:5000/";
    // MUC:
    //final public static String baseUrl = "http://192.168.178.21:5000/";
    //final public static String baseUrl = "http://192.168.178.39:5000/";
    final private static String LOG_TAG = "MainActivity";
    public static RequestQueue queue;

    public static User user_acc;
    public static UserViewModel userViewModel;


    public static void toast(final Context context, final String text) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(() -> Toast.makeText(context, text, Toast.LENGTH_LONG).show());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        queue = Volley.newRequestQueue(getApplicationContext());
        createDB(getApplicationContext());
        DiriDatabase diriDatabase = DiriDatabase.getInstance(getApplicationContext());

        userViewModel = new UserViewModel(getApplication());
        userViewModel.getUser().observe(this, user -> {
            Log.i(LOG_TAG, "load user");
            if (user != null) {
                Log.i(LOG_TAG, "user exists: " + user.id);
                user_acc = user;
                return;
            }
            Log.i(LOG_TAG, "No user found-> request initial data");
            requestInitialData();
        });

        final Button speakButton = findViewById(R.id.buttonSpeak);
        speakButton.setOnClickListener(v -> launchRecActivity());
        final Button spellButton = findViewById(R.id.buttonListenAndWrite);
        spellButton.setOnClickListener(v -> launchSpellActivity());

        final Button profileButton = findViewById(R.id.buttonProfile);
        profileButton.setOnClickListener(v -> launchUserProfile());

        //final Button improveButton = findViewById(R.id.buttonImprove);
        //improveButton.setOnClickListener(v -> launchAu);

        final Button infoButton = findViewById(R.id.buttonInfo);
        infoButton.setOnClickListener(v -> launchInfoActivity());

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){

            // this will request for permission when user has not granted permission for the app
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        }
    }

    private void requestInitialData() {
        // TODO: move to userrepository
        String url = MainActivity.baseUrl + "user?gamelang=de&userlang=de";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                response -> {
                    //textView.setText("Response is: "+ response.substring(0,500));
                    //Log.w(LOG_TAG, "Pronunciations loaded: " + response);
                    try {
                        JSONObject responseObj = new JSONObject(response);
                        Log.i(LOG_TAG, responseObj.toString());
                        String id = responseObj.getString("id");
                        String gamelang = responseObj.getString("gamelang");
                        String userlang = responseObj.getString("userlang");
                        User newUser = new User(id, gamelang, userlang);
                        userViewModel.insertUser(newUser);
                        user_acc = newUser;
                        SpellingViewModel spellingViewModel = new SpellingViewModel(getApplication());
                        spellingViewModel.downloadNumberOfSpellings(5);
                        PronunciationViewModel pronunciationViewModel = new PronunciationViewModel(getApplication());
                        pronunciationViewModel.downloadPronunciations();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }, error -> {
            toast(getApplicationContext(), "Account creation failed, please check your Internet connection!");
        });
        queue.add(stringRequest);
    }

    private void launchRecActivity() {
        Intent intent = new Intent(this, RecActivity.class);
        startActivity(intent);
    }

    private void launchSpellActivity() {
        Intent intent = new Intent(this, SpellActivity.class);
        startActivity(intent);
    }

    private void launchUserProfile() {
        Intent intent = new Intent(this, UserProfile.class);
        startActivity(intent);
    }

    private void launchInfoActivity() {
        Intent intent = new Intent(this, InfoActivity.class);
        startActivity(intent);
    }

    public void createDB(Context applicationContext) {
        Room.databaseBuilder(applicationContext,
                DiriDatabase.class, "diri_database").build();
        Log.v(LOG_TAG, "Created DB");
    }

}