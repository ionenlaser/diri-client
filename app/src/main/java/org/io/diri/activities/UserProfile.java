package org.io.diri.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;


import org.io.diri.R;


public class UserProfile extends AppCompatActivity {
    private TextView userpointsView;
    private  TextView userRecView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        userpointsView = findViewById(R.id.pointCountView);
        userpointsView.setText(String.valueOf(MainActivity.user_acc.getPoints()));

        userRecView = findViewById(R.id.reCounter);
        userRecView.setText(String.valueOf(MainActivity.user_acc.getRecordings()));
    }

}
