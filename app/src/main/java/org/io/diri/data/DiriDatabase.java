package org.io.diri.data;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import org.io.diri.data.dao.PronunciationDao;
import org.io.diri.data.dao.SpellingDao;
import org.io.diri.data.dao.UserDao;
import org.io.diri.data.entities.Pronunciation;
import org.io.diri.data.entities.Spelling;
import org.io.diri.data.entities.User;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@androidx.room.Database(entities = {User.class, Pronunciation.class, Spelling.class}, version = 22)
public abstract class DiriDatabase extends RoomDatabase{
    private static final String LOG_TAG = "Database";
    private static volatile DiriDatabase INSTANCE;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseExecutor =
            Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public abstract UserDao userDao();
    public abstract SpellingDao spellingDao();
    public abstract PronunciationDao pronunciationDao();

    public static DiriDatabase getInstance(final Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    DiriDatabase.class, "diri_database")
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return INSTANCE;
    }
}