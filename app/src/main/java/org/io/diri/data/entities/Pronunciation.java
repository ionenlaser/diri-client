package org.io.diri.data.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;



@Entity(tableName = "pronunciation_table", indices = {@Index(value = {"server_id"},unique = true)})
public class Pronunciation {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public int id;

    @ColumnInfo(name = "server_id")
    public int server_id;

    @ColumnInfo(name = "ipa")
    public String ipa;

    @ColumnInfo(name = "lexeme")
    public String lexeme;

    @ColumnInfo(name = "recording")
    public String recording;

    @ColumnInfo(name = "uploaded")
    public boolean uploaded;

    @ColumnInfo(name = "language")
    public String language;

    @ColumnInfo(name = "skipped")
    public Boolean skipped;


    public Pronunciation(Integer server_id, String ipa, String lexeme, String language) {
        this.setServer_id(server_id);
        this.setIpa(ipa);
        this.setLexeme(lexeme);
        this.setLanguage(language);
    }

    public int getServer_id() {
        return server_id;
    }

    public void setServer_id(int server_id) {
        this.server_id = server_id;
    }

    public String getLanguage() { return language; }
    public void setLanguage(String language) { this.language = language; }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getIpa() {
        return ipa;
    }
    public void setIpa(String ipa) {
        this.ipa = ipa;
    }

    public String getLexeme() {
        return lexeme;
    }
    public void setLexeme(String lexeme) {
        this.lexeme = lexeme;
    }

    public String getRecording() {
        return recording;
    }
    public void setRecording(String recording) {
        this.recording = recording;
    }

    public boolean isUploaded() {
        return uploaded;
    }
    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public Boolean getSkipped() { return skipped; }
    public void setSkipped(Boolean skipped) { this.skipped = skipped; }
}
