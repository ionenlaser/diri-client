package org.io.diri.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import org.io.diri.data.entities.Spelling;

import java.util.List;

@Dao
public interface SpellingDao {
    @Query("SELECT * FROM spelling_table WHERE skipped is NULL AND lexeme is not NULL")
    LiveData<List<Spelling>> loadUnplayedSpellingData();

    @Query("SELECT * FROM spelling_table WHERE uploaded is not 1 AND (inputs is not NULL OR skipped is 1)")
    List<Spelling> loadNotUploadedSpellings();

    @Query("SELECT * FROM spelling_table WHERE :serverIDParam = serverid LIMIT 1")
    Spelling loadSpellingByServerID(int serverIDParam);

    @Update
    void updateSpelling(Spelling spelling);

    @Insert
    void insertSpelling(Spelling spelling);

    @Delete
    void deleteSpelling(Spelling spelling);
}

