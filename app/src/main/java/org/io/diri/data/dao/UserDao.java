package org.io.diri.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import org.io.diri.data.entities.User;

@Dao
public interface UserDao {
    @Query("SELECT * FROM user_table")
    LiveData<User> loadUserData();

    @Update
    void updateUser(User user);

    @Insert
    void insertUser(User user);
}
