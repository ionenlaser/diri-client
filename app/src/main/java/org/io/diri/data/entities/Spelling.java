package org.io.diri.data.entities;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "spelling_table")
public class Spelling {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public int id;

    @ColumnInfo(name = "uploaded")
    public boolean uploaded;

    @ColumnInfo(name = "serverid")
    public int serverid;

    @ColumnInfo(name = "recording")
    public String recording;

    @ColumnInfo(name = "correctionrec")
    public String correctionrec;

    @ColumnInfo(name = "corrected")
    public Boolean corrected;

    @ColumnInfo(name = "lexeme")
    public String lexeme;

    @ColumnInfo(name = "ipa")
    public String ipa;

    @ColumnInfo(name = "language")
    public String language;

    @ColumnInfo(name = "inputs")
    public String inputs;

    @ColumnInfo(name = "skipped")
    public Boolean skipped;

    @ColumnInfo(name = "status") //evaluation of recording (kind of feedback "good", bad")
    public String status;

    public Spelling(int serverid, String recording) {
        this.setServerid(serverid);
        this.setRecording(recording);
    }


    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRecording() {
        return recording;
    }

    public void setRecording(String recording) {
        this.recording = recording;
    }

    public String getCorrectionrec() {
        return correctionrec;
    }

    public void setCorrectionrec(String correctionrec) {
        this.correctionrec = correctionrec;
    }

    public Boolean getCorrected() {
        return corrected;
    }

    public void setCorrected(Boolean corrected) {
        this.corrected = corrected;
    }

    public String getLexeme() {
        return lexeme;
    }

    public void setLexeme(String lexeme) {
        this.lexeme = lexeme;
    }

    public String getIpa() {
        return ipa;
    }

    public void setIpa(String ipa) {
        this.ipa = ipa;
    }

    public String getInputs() {
        return inputs;
    }

    public void setInputs(String inputs) {
        this.inputs = inputs;
    }

    public boolean isSkipped() {
        return skipped;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getServerid() {
        return serverid;
    }

    public void setServerid(int serverid) {
        this.serverid = serverid;
    }

    public boolean isUploaded() {
        return uploaded;
    }

    public void setUploaded(boolean uploaded) {
        this.uploaded = uploaded;
    }

    public Boolean getSkipped() {
        return skipped;
    }

    public void setSkipped(boolean skipped) {
        this.skipped = skipped;
    }
}




