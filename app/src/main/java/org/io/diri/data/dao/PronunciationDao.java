package org.io.diri.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import org.io.diri.data.entities.Pronunciation;

import java.util.List;


@Dao
public interface PronunciationDao {
    @Query("SELECT * FROM pronunciation_table WHERE skipped is NULL")
    LiveData<List<Pronunciation>> loadUnplayedPronunciationData();

    @Update
    void updatePronunciation(Pronunciation pronunciation);

    @Insert
    void insertPronunciation(Pronunciation pronunciation);

    @Delete
    void deletePronunciation(Pronunciation pronunciation);
}
