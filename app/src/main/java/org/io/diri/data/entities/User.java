package org.io.diri.data.entities;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


@Entity(tableName = "user_table")
public class User {
    @NonNull
    @PrimaryKey()
    @ColumnInfo(name = "id")
    public String id;

    @ColumnInfo(name = "userlang")
    public String userlang;

    @ColumnInfo(name = "gamelang")
    public String gamelang;

    @ColumnInfo(name = "points")
    public int points;

    @ColumnInfo(name = "recordings")
    public int recordings;

    public User(String id, String gamelang, String userlang) {
        this.id = "";
        this.setId(id);
        this.setGamelang(gamelang);
        this.setUserlang(userlang);
        this.setPoints(0);
        this.setRecordings(0);
    }


    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getUserlang() { return userlang; }
    public void setUserlang(String userlang) { this.userlang = userlang; }

    public String getGamelang() { return gamelang; }
    public void setGamelang(String gamelang) { this.gamelang = gamelang; }

    public int getPoints() { return points; }
    public void setPoints(int points) { this.points = points; }

    public int getRecordings() { return recordings; }
    public void setRecordings(int recordings) { this.recordings = recordings; }
}


