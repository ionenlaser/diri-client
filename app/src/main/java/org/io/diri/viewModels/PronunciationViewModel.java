package org.io.diri.viewModels;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.io.diri.activities.MainActivity;
import org.io.diri.data.DiriDatabase;
import org.io.diri.data.entities.Pronunciation;
import org.io.diri.repositories.PronunciationRepository;
import org.json.JSONException;

import java.io.IOException;
import java.util.List;

public class PronunciationViewModel extends AndroidViewModel {

    private static final String LOG_TAG = "PronunciationViewModel";
    private final PronunciationRepository pronunciationRepository;
    private final LiveData<List<Pronunciation>> unplayedPronunciations;

    public PronunciationViewModel(Application application) {
        super(application);
        pronunciationRepository = new PronunciationRepository(application);
        unplayedPronunciations = pronunciationRepository.getUnplayedPronunciations();
    }

    public LiveData<List<Pronunciation>> getUnplayedPronunciations() {
        return unplayedPronunciations;
    }

    public void insert(Pronunciation pronunciation) {
        pronunciationRepository.insert(pronunciation);
    }

    public void update(Pronunciation pronunciation) {
        pronunciationRepository.update(pronunciation);
    }

    public void downloadPronunciations() {
        try {
            pronunciationRepository.downloadPronunciationData();
        } catch (Exception e) {
            MainActivity.toast(getApplication(), "Failing getting recording Data!");
            Log.e(LOG_TAG, "Failing getting recording Data!");
            Log.e(LOG_TAG, e.toString());
        }

    }

    public void postPronunciations(Pronunciation pronunciation) {
        DiriDatabase.databaseExecutor.execute(() -> {
            Log.i(LOG_TAG, "Uploading new pronunciation");
            try {
                pronunciationRepository.postPronunciationData(pronunciation);
            } catch (IOException e) {
                Log.e(LOG_TAG, "Pronunciation upload was unsuccessful! (IOException)");
                Log.e(LOG_TAG, e.toString());
            } catch (JSONException e) {
                Log.e(LOG_TAG, "Pronunciation upload was unsuccessful! (JSONException)");
                Log.e(LOG_TAG, e.toString());
                MainActivity.toast(getApplication(), "Recoding upload was unsuccessful!");
            }
        });
    }
}
