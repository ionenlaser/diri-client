package org.io.diri.viewModels;

import android.app.Application;
import android.util.Log;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.io.diri.activities.MainActivity;
import org.io.diri.data.DiriDatabase;
import org.io.diri.data.entities.Spelling;
import org.io.diri.repositories.SpellingRepository;

import java.util.List;

public class SpellingViewModel extends AndroidViewModel {

    private final SpellingRepository spellingRepository;
    private final LiveData<List<Spelling>> allSpellings;
    private static final String LOG_TAG = "SpellingViewModel";

    public SpellingViewModel(Application application) {
        super(application);

        spellingRepository = new SpellingRepository(application);
        allSpellings = spellingRepository.getUnplayedSpellings();
    }

    public LiveData<List<Spelling>> getAllSpellings() {
        return allSpellings;
    }

    public void insert(Spelling spelling) {
        spellingRepository.insert(spelling);
    }

    public void update(Spelling spelling) {
        spellingRepository.update(spelling);
    }

    public void downloadNumberOfSpellings(int number) {
        for (int i = 0; i < number; i++) {
            downloadNewSpelling();
        }
    }

    private void downloadNewSpelling() {
        DiriDatabase.databaseExecutor.execute(() ->{
            try {
                Log.i(LOG_TAG, "Downloading new spelling");
                spellingRepository.downloadSpelling();
            } catch(Exception e) {
                MainActivity.toast(getApplication(), "Spelling download was unsuccessful!");
            }
        });
    }

    public void postSpellingData() {
        spellingRepository.postSpellingInput();
    }

    /*public void getAdditionalInfo(Spelling spelling) {
        spellingRepository.getAdditionalInfo(spelling);
    }*/
}
