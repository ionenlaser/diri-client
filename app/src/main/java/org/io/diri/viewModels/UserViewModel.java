package org.io.diri.viewModels;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.io.diri.data.entities.User;
import org.io.diri.repositories.UserRepository;

public class UserViewModel extends AndroidViewModel {

    private final UserRepository userRepository;
    private final LiveData<User> user;

    public UserViewModel(Application application) {
        super(application);

        userRepository = new UserRepository(application);
        user = userRepository.getAllUsers();
    }

    public LiveData<User> getUser() {
        return user;
    }

    public void insertUser(User user) {
        userRepository.insertUser(user);
    }

    public void updateUser(User user) {
        userRepository.updateUser(user);
    }
}

